# fedora-kinoite

Chris' learnings on how to get a Fedora Kinoite workstation operational for his use.

- Available officially since the release of Fedora 35.
  - [Fedora nightly builds](https://openqa.fedoraproject.org/nightlies.html) for those that want to live on the bleeding edge
- nVidia binary drivers work, but Chris has encountered issues with upgrades. When this happens, do the following:

  1. Reset / remove overlay packages ```rpm-ostree reset --overlays```
  1. Upgrade ostree ```rpm-ostree upgrade```
  1. Reinstall repos (look below on method)
  1. Resintall  overlay packages (look below on method)

- Cockpit machines doesn't have a nice UI for inserting ignition files into cloud-init for CoreOS guests.
  - [Issue 431](https://github.com/cockpit-project/cockpit-machines/issues/431) now has a mention of the problem.
  - Workaround is likely to be:
    - stand up web service with ignition files on local machine (nginx + ignition files), boot using iso and point installer to ignition
    - use [ansible virt module](https://docs.ansible.com/ansible/latest/collections/community/libvirt/virt_module.html)
      - [Example from RedHat](https://www.redhat.com/sysadmin/build-VM-fast-ansible)

    ```shell
    # List available VMs
    ansible localhost -b -m virt -a 'command=list_vms'

    # Get XML of an existing VM (host = test in this example)
    ANSIBLE_LOAD_CALLBACK_PLUGINS=yes \
    ANSIBLE_STDOUT_CALLBACK=json \
    ansible localhost -b -m virt -a 'command=get_xml name=test' | \
      jq -r '.plays[0].tasks[0].hosts.localhost.get_xml'
    ```

## Overview

Key concept is to maximise interaction and use of rpm-ostree and container centric application deployment.

Ideally all apps will obey the kde keyboard shortcuts, to allow consistent cut/copy/paste keys...
- Firefox doesn't
- Chrome does!  :)

### Chris' workstation needs

- Hardware support
  - nvidia driver
- Terminal
- Mail client with GPG support
- Calendar with ical support
- Contacts with carddav support
- VSCode (>=1.61 required)
- Web browsers
  - Vivaldi (preferred)
  - Google Chrome
  - Firefox
- gpg
- terminal tools
  - htop
  - jq
  - pv

## Method

```shell
# Add repos to rpm-ostree
# NOTE: fedora-workstation-repositories seems broken in the pre-beta as at 20210827
#       https://fedoraproject.org/wiki/Workstation/Third_Party_Software_Repositories
FEDORA_RELEASE=$(rpm -E %fedora)
rpm-ostree install --idempotent \
    https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-${FEDORA_RELEASE}.noarch.rpm \
    https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-${FEDORA_RELEASE}.noarch.rpm \
    fedora-workstation-repositories
wget -O /etc/yum.repos.d/enpass.repo https://yum.enpass.io/enpass-yum.repo
wget -O /etc/yum.repos.d/vivaldi.repo https://repo.vivaldi.com/archive/vivaldi-fedora.repo

# Google Chrome should be available from fedora-workstation-repositories, but since that is currently broken, add manually
for repo in vscode google-chrome
do
  wget -O /etc/yum.repos.d/${repo}.repo \
      https://gitlab.com/chrisweeksnz/fedora-kinoite/-/raw/main/ostree-repos/${repo}.repo
done

# Reboot to enable repos
systemctl reboot

# Add nVidia driver
# - https://medium.com/@egee_irl/install-nvidia-drivers-on-silverblue-edd499244aa2
# - https://rpmfusion.org/Howto/NVIDIA#Silverblue
rpm-ostree install --idempotent \
    akmod-nvidia xorg-x11-drv-nvidia \
    akmod-nvidia xorg-x11-drv-nvidia-cuda \
    xorg-x11-drv-nvidia-libs
#    xorg-x11-drv-nvidia-libs.i686 may be needed for steam, but can't be installed as at 20210827
rpm-ostree kargs \
    --append=rd.driver.blacklist=nouveau \
    --append=modprobe.blacklist=nouveau \
    --append=nvidia-drm.modeset=1 \
    --reboot

# Enable pipewiree on user systemd for audio to work (as user in konsole)
systemctl --user enable --now pipewire-media-session.service
# Note you might need to set the audio profile for the HDMI audio output, if using nVidia proprietory drivers
# - system -> audio -> playback devices -> check show inactive devices
# - system -> audio -> playback devices -> configure -> TU106 High Definition Audio Controller -> Profile: Digital Stereo Output

# Install ostree overlays
# - https://www.if-not-true-then-false.com/2010/install-google-chrome-with-yum-on-fedora-red-hat-rhel/
# Still need to make Chrome and Vivaldi work
# - [lsb could be needed for Chrome](https://www.unixmen.com/fix-google-chrome-dependencies-installing-via-rpm-package/)
# - systemd 249 seems to be broken with webkit browsers
#   - https://bugzilla.redhat.com/show_bug.cgi?id=1973461
#   - https://bugs.chromium.org/p/chromium/issues/detail?id=1221442
#   - https://ubuntu.forumming.com/question/12510/chrome-is-not-starting-in-ubuntu-20-04-error-sandbox-linux-cc-374-initializesandbox-called-with-multiple-threads-in-process-gpu-process

rpm-ostree install --idempotent \
    adobe-source-sans-pro-fonts \
    ansible \
    clamav \
    clamav-update \
    clamd \
    cockpit \
    cockpit-machines \
    cockpit-networkmanager \
    cockpit-ostree \
    cockpit-pcp \
    cockpit-podman \
    cockpit-selinux \
    cockpit-storaged \
    cockpit-system \
    code \
    docker-compose \
    enpass \
    google-chrome-stable \
    google-droid-fonts-all \
    google-roboto-fonts \
    htop \
    ibm-plex-fonts-all \
    jetbrains-mono-fonts-all \
    kamoso \
    kate \
    kate-plugins \
    kde-partitionmanager \
    kontact \
    levien-inconsolata-fonts \
    libguestfs-tools \
    libvirt \
    linux-libertine-fonts \
    lsb \
    lynis \
    mozilla-fira-mono-fonts \
    mozilla-fira-sans-fonts \
    neofetch \
    pam-u2f \
    pam_yubico \
    plasma-nm-l2tp \
    plasma-nm-ssh \
    podman-docker \
    qemu-kvm \
    redhat-display-fonts \
    redhat-mono-fonts \
    redhat-text-fonts \
    rkhunter \
    terminus-fonts \
    virt-install \
    vivaldi-stable \
    yubikey-manager-qt \
    yubikey-personalization-gui \
    yubioath-desktop \
    ykpers \
    wireguard-tools

# Disable warning about podman emulation of docker
touch /etc/containers/nodocker

# Install flatpak applications
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub com.visualstudio.code # for the MS edition
#flatpak run com.visualstudio.code-oss # for the opensource edition
flatpak install flathub org.remmina.Remmina
flatpak install flathub com.getpostman.Postman
flatpak install flathub com.bitwarden.desktop
flatpak install flathub com.prusa3d.PrusaSlicer
```

Reboot host to enable ostree overlays

```shell
systemctl reboot
```

Ensure selinux labels are correct for libvirt
- this can be repeated at any time

```shell
sbin/restorecon -R -vF /var/lib/libvirt
```

Start and enable any installed services

```shell
# fix pcscd service file for yubikey manager
sed 's|@sbindir_exp@/pcscd|/usr/sbin/pcscd|' /usr/lib/systemd/system/pcscd.service > /etc/systemd/system/pcscd.service

for service in cockpit.socket libvirtd.socket pcscd.socket podman.socket
do 
  systemctl enable --now ${service}
done
```

Set SELinux policies

```shell
# Move to build/storage directory
mkdir -p /root/selinux
cd /root/selinux

# Make policies
for policy in firewalld systemd-gpt-aut pmdakvm pmie
do
  ausearch -c ${policy} --raw | audit2allow -M my-${policy}
  semodule -X 300 -i my-${policy}.pp
done

# Apply policies
for module in $(ls -1 /root/selinux/*.pp)
do 
  semodule -i ${module}
done
```

[Enpass extension for browsers](https://www.enpass.io/downloads/#extensions)
- [Click on here to install on Mozilla Firefox](https://dl.enpass.io/stable/extensions/firefox/versions/v6.6.2-9/enpass-firefox-6.6.2.xpi)

System settings:
- Key bindings in settings (copy, cut, paste, select all, undo, save, quit, open klipper at mouse position, show activity switcher)
- energy saver: screen off after 30 min
- Appearance: Breeze Dark
- Window Management -> Task Switcher -> Visualization: Grid

Firefox settings:
- Addons & Themes -> Find more addons: Breeze Dark
  - Install Breeze Dark by schlagma
- Addons & Themes -> Themes -> Breeze Dark

Google Chrome:
- Settings -> Appearance -> Theme -> GTK+
- Settings -> Appearance -> Use system title bar and borders -> Enable
- Add [KDE Plasma Integration](https://chrome.google.com/webstore/detail/empty-title/cimiefiiaegbelhefglklhhakcgmhkai?hl=en-GB)
- Settings -> On start-up -> "Continue where you left off"

[VSCode keybindings](https://code.visualstudio.com/docs/getstarted/keybindings):
- Use meta-* (copy, cut, paste, select all, undo, save, quit, command palette)
- Install and use andrewfridley.breeze-dark-theme

Taskbar/panel settings:
- Set panel to top of screen
- Configure digital clock:
  - show seconds
  - only show time zone when different from local time
  - date format: long date
- Make dolphin better by installing
  - kdiskfree
  - kpartitionmanager
- hack font
  - https://copr.fedorainfracloud.org/coprs/zawertun/hack-fonts/
  - not yet packaged for Fedora (mind blown)

Configure Chrome
1. Open Google Chrome browser on Windows, Mac, Linux, or Chromebook.
1. Visit the URL ```chrome://flags/#enable-reader-mode``` in new tab.
1. Enable the option “Enable Reader Mode.”
1. Click the “Relaunch” button that appears at the bottom to relaunch Google Chrome.
1. Now you can visit any website or webpage with long readable text.
1. Click on the book icon on the right of the address bar to enable reader mode.


Configure lynix, rkhunter and clamav

```shell
# check existing install
# note: Suckit Rootkit (additional checks) will alert on ostree systems for multiple 
#       hardlinks on /sbin/init (find / -samefile /sbin/init):
#     - /sysroot/ostree/repo/objects/../<uid>.file
#     - /sysroot/ostree/deploy/fedora/deploy/<uid>/usr/sbin/init
#     - /usr/sbin/init
rkhunter --update
rkhunter --check --skip-keypress

# Run audit and apply and relevant changes
lynis audit system

# Harden system as per lynis recommendations
cp /etc/lynis/default.prf /etc/lynis/default.prf.original
## If you are using libvirt bridged interfaces, then exclude from scan. Interface enp6s0 used in this example.
sed -i '/^#if_promisc/ a\
if_promisc:enp6s0:libvirt bridging interface:' /etc/lynis/default.prf

# Update av signatures and scan system
freshclam
for directory in /home /var
do
  clamscan --infected --remove --recursive ${directory}
done

# Configure clamd
# see:
# - https://www.hiroom2.com/2018/11/25/fedora-29-clamav-en/
# - https://www.ctrl.blog/entry/how-to-periodic-clamav-scan.html

# Configure systemd timers to check the system
```

#### Resources

- https://discussion.fedoraproject.org/t/kinoite-a-kde-and-now-xfce-version-of-fedora-silverblue/147/286
- https://mutschler.eu/linux/install-guides/fedora-post-install/
- https://fedoramagazine.org/5-great-monospaced-fonts-for-coding-and-the-terminal-in-fedora/
- https://fedoramagazine.org/use-docker-compose-with-podman-to-orchestrate-containers-on-fedora/
- https://www.redhat.com/sysadmin/podman-docker-compose
- https://cisofy.com/documentation/lynis
- https://docs.clamav.net/
- https://sourceforge.net/p/rkhunter/wiki/index/
- https://docs.fedoraproject.org/en-US/fedora-server/virtualization-install/
- https://medium.com/@alex285/how-to-enable-h-264-on-chromium-on-silverblue-6ca8058a6a27
- [Things to do after installing Fedora Workstation](https://mutschler.eu/linux/install-guides/fedora-post-install/)
- https://www.ctrl.blog/entry/how-to-periodic-clamav-scan.html
- https://www.hiroom2.com/2018/11/25/fedora-29-clamav-en/
