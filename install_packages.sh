
# Reset / remove overlay packages
#rpm-ostree reset --overlays

# Upgrade ostree 
#rpm-ostree upgrade

# Reinstall repos
rpm-ostree install --apply-live \
  https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
  https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
wget -O /etc/yum.repos.d/enpass.repo https://yum.enpass.io/enpass-yum.repo
wget -O /etc/yum.repos.d/vivaldi.repo https://repo.vivaldi.com/archive/vivaldi-fedora.repo
# Google Chrome should be available from fedora-workstation-repositories, but since that is currently broken, add manually
for repo in vscode google-chrome
do
  wget -O /etc/yum.repos.d/${repo}.repo \
      https://gitlab.com/chrisweeksnz/fedora-kinoite/-/raw/main/ostree-repos/${repo}.repo
done

# Reinstall overlay packages
rpm-ostree install --idempotent \
adobe-source-sans-pro-fonts \
ansible \
clamav \
clamav-update \
clamd \
cockpit \
cockpit-machines \
cockpit-networkmanager \
cockpit-ostree \
cockpit-pcp \
cockpit-podman \
cockpit-selinux \
cockpit-storaged \
cockpit-system \
code \
docker-compose \
enpass \
google-chrome-stable \
google-droid-fonts-all \
google-roboto-fonts \
htop \
ibm-plex-fonts-all \
jetbrains-mono-fonts-all \
kamoso \
kate \
kate-plugins \
kontact \
kwalletcli \
levien-inconsolata-fonts \
libguestfs-tools \
libvirt \
linux-libertine-fonts \
lsb \
lynis \
mozilla-fira-mono-fonts \
mozilla-fira-sans-fonts \
neofetch \
pam_yubico \
pam-u2f \
plasma-nm-l2tp \
plasma-nm-ssh \
podman-docker \
qemu-kvm \
redhat-display-fonts \
redhat-mono-fonts \
redhat-text-fonts \
rkhunter \
terminus-fonts \
virt-install \
vivaldi-stable \
wireguard-tools \
xorg-x11-drv-nvidia \
xorg-x11-drv-nvidia-cuda \
xorg-x11-drv-nvidia-libs \
ykpers \
yubikey-manager-qt \
yubikey-personalization-gui \
yubioath-desktop
